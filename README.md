# Proj32

## Overview

Proj32 is a TypeScript application built on the AWS Cloud Development Kit (CDK) to deploy a serverless architecture for handling image uploads to an S3 bucket and processing them with AWS Lambda.

## Features

- **Serverless Architecture**: Utilizes AWS Lambda for serverless computing, eliminating the need for provisioning or managing servers.
- **Image Uploads**: Allows users to upload images to an S3 bucket.
- **Image Processing**: Automatically processes uploaded images using AWS Lambda functions.
- **Environment Configuration**: Configures Lambda function environment variables for region settings and thumbnail sizes.

## Requirements

- Node.js
- AWS CLI configured with appropriate permissions
- AWS CDK installed

## Installation

1. Clone the repository.
2. Navigate to the project directory.
3. Run `npm install` to install dependencies.
4. Configure your AWS credentials using `aws configure` if not already set up.
5. Deploy the application using `cdk deploy`.

## Usage

Once deployed, the application provides endpoints for uploading images to the configured S3 bucket. The Lambda function automatically processes these images based on the provided configuration.

## CodeWhisperer Usage

CodeWhisperer was instrumental in ensuring the quality and maintainability of the TypeScript codebase. Here's how it was specifically used:

1. **Import Statement Management**: CodeWhisperer helped manage import statements efficiently, ensuring that only necessary modules were imported and that they were organized properly.

2. **Code Structure Optimization**: It assisted in structuring the codebase optimally, ensuring readability and maintainability.

3. **Syntax and Best Practices Enforcement**: CodeWhisperer enforced TypeScript syntax and best practices throughout the development process, ensuring consistency and adherence to coding standards.

4. **Type Safety Assurance**: It helped in maintaining type safety throughout the application, reducing the likelihood of runtime errors.

5. **Refactoring Support**: CodeWhisperer provided support for refactoring tasks, making it easier to make changes to the codebase without introducing bugs.

Overall, CodeWhisperer significantly contributed to the development of a robust and efficient TypeScript application, enabling smoother development and enhancing code quality.
