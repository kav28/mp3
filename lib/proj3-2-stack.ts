import * as path from 'path'; // Import the path module

import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { Code, Function, Runtime } from 'aws-cdk-lib/aws-lambda';
import * as s3 from 'aws-cdk-lib/aws-s3';
import { RemovalPolicy } from 'aws-cdk-lib';

export class Proj32Stack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);
      
    const handler = new Function(this, 'new-lambdafor-s3', {
      runtime : Runtime.NODEJS_20_X,
      timeout : cdk.Duration.seconds(30),
      handler : 'app.handler',
      code: Code.fromAsset(path.join(__dirname,'../lambdas')), // Use path.join to construct the file path
      environment : {
        REGION_NAME : "us-east-1",
        THUMBNAIL_SIZE : "128"
      }
    });

    const news3Bucket = new s3.Bucket(this, 'image-bucket', {
        removalPolicy : cdk.RemovalPolicy.DESTROY,
        autoDeleteObjects : true
    })

    news3Bucket.grantReadWrite(handler); // aws lambda
  }
}

